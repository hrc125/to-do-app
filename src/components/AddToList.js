import React from "react"

export const AddToList = (props) => {
    const[todo, setTodo] = React.useState("")
    const[error, setError] = React.useState("")
    const handleChange = (e) => setTodo(e.target.value)
    const handleSubmit = (e) => {
        e.preventDefault()
        if(todo !== ""){
            props.onSubmit({
                id: new Date().getTime(),
                text: todo,
                complete: false
            })
            setTodo("")
            setError("")
        }
        else{
            setError("*Please enter something")
        }
    }
    return (
    <div>
        <form onSubmit={handleSubmit}>
        <input value={todo} 
                onChange={handleChange} 
                placeholder="Add a new task"/>
        <button type="submit">Add Task</button>
    </form>
    {error? <h6>{error}</h6>: null}
    </div>

    )    
}