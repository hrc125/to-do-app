import React from "react"

export const ToDoList = (props) => {
    return(
        <div>
            {props.todoItem.text}
            <button onClick={props.handleToggle}>
                Mark as {props.todoItem.complete? "Incomplete":"Complete"}
            </button>
        </div>
    )
}