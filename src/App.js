import React from 'react';
import './App.css';
import { AddToList } from './components/AddToList'
import { ToDoList } from './components/TodoList';

function App() {
  const[todos, setTodos] = React.useState([])
  const[show, setShow] = React.useState(0)
  const onAddItem = (todo) => setTodos([todo, ...todos])
  const onToggle = (id) => {
    setTodos(todos.map((todoItem) => {
            if(todoItem.id === id){
                return {
                  ...todoItem,
                  complete: !todoItem.complete,
                }
              }
              return todoItem
      })
    )
  }
  let todosList = []
  if(show === 0){
    todosList = todos
  }
  else if(show === 1){
    todosList = todos.filter(todo => todo.complete)
  }
  else if(show === 2){
    todosList = todos.filter(todo => !todo.complete)
  }
  return (
    <div className="App">
      <header className="App-header">
        <h2>To do List</h2>
        <AddToList onSubmit={onAddItem}/>
        <div>
          <button onClick={() => setShow(0)}>All</button>
          <button onClick={() => setShow(1)} >Completed</button>
          <button onClick={() => setShow(2)}>Incomplete</button>
        </div>
        {todosList.map((todoItem) => 
          <ToDoList key={todoItem.id} 
                    todoItem={todoItem} 
                    handleToggle={() => onToggle(todoItem.id)}/>
        )}
      </header>
    </div>
  );
}

export default App;
